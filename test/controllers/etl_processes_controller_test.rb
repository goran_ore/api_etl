require 'test_helper'

class EtlProcessesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @etl_process = etl_processes(:one)
  end

  test "should get index" do
    get etl_processes_url, as: :json
    assert_response :success
  end

  test "should create etl_process" do
    assert_difference('EtlProcess.count') do
      post etl_processes_url, params: { etl_process: { ended_at: @etl_process.ended_at, farm_id: @etl_process.farm_id, started_at: @etl_process.started_at, status: @etl_process.status, user_id: @etl_process.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show etl_process" do
    get etl_process_url(@etl_process), as: :json
    assert_response :success
  end

  test "should update etl_process" do
    patch etl_process_url(@etl_process), params: { etl_process: { ended_at: @etl_process.ended_at, farm_id: @etl_process.farm_id, started_at: @etl_process.started_at, status: @etl_process.status, user_id: @etl_process.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy etl_process" do
    assert_difference('EtlProcess.count', -1) do
      delete etl_process_url(@etl_process), as: :json
    end

    assert_response 204
  end
end
