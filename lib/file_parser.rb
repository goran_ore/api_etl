class FileParser

  # initialize - Accepts Array of file paths. Sets file paths in local variable and initialize results variable
  # also intialize master result variable:
  # => hash: results<key: file_path, value: table_hash>
  def initialize(file_paths = nil)
    unless file_paths.nil?
      @file_paths = file_paths
      @results = Hash.new
      parse_files
    end
  end

  # Add additional file paths for parsing.
  def add_path(path)
    unless @file_paths.include?(path)
      @file_paths.push(path)
      parse_files
    end
  end

  # parse_files - Loop through files list and initiates appropriate parser methods
  def parse_files
    #Loop through paths, if any
    if @file_paths.any?
      @file_paths.each do |path|
        # Parse only files that are not already parsed
        unless @results.has_key?(path)
          Rails.logger.debug "FileParser:parse_files Parsing of file: #{path} starting..."
          pathObj = Pathname.new(path)

          if pathObj.file?
            if pathObj.extname.downcase == ".xls"
              @results[path] = readxls(path)
            elsif pathObj.extname.downcase == ".xlsx"
              @results[path] = readxlsx(path)
            elsif pathObj.extname.downcase == ".dif"
              @results[path] = readdif(path)
            else
              @results[path] = readtxt(path)
            end
          end
        end
      end
    end
    return @results
  end

  # get_results - Accepts collection of keys of the results to return. If nothing is provided return all results. example: @results["d:\lalal.dif"][3][3]
  def get_results(key = nil)
    if !key.nil?

      case key
      when Array
        return @results.values_at(*key).compact
      else
        if !@results.nil? && @results.has_key?(key)
          return @results[key]
        end
      end
    else
      return @results
    end
  end

  private
  # <individual parser methods> - for each xls, xlsx, txt and dif individual parser. Methods will accept one path, and give a result in a hash.
  # => data results will be represented in:
  # => hash: table_hash <key: row_number, value: row_hash> and a row_hash will hold row values as:
  # => hash: row_hash <key: cell_number, value: cell_value>
  def readxls(path)
    Rails.logger.debug "FileParser:parse_files: .xls file parsing..."
    table_hash = Hash.new
    workbook = Roo::Spreadsheet.open(path)
    worksheetsNames = workbook.sheets
    Rails.logger.debug "FileParser:parse_files: .xls worksheets: #{worksheetsNames.inspect}"
    # Loop through sheets
    worksheetsNames.each do |worksheetName|
      #Rails.logger.debug "FileParser:parse_files: .xls worksheetName: #{worksheetName.inspect}"
      row_number = 0
      # Here begins all rows
      rows = workbook.sheet(worksheetName)
      rows.each do |row|
        #Rails.logger.debug "FileParser:parse_files: .xls row: #{row.inspect}"
        row_hash = Hash.new  #row results

        # Loop through cells
        (0..row.count).each do |i|
          row_hash[i+1] = row[i].to_s
        end

        table_hash[row_number] = row_hash
        row_number += 1
      end
    end
    Rails.logger.debug "FileParser:parse_files: .xls result row count: #{table_hash.count}"
    return table_hash
  end

  def readxlsx(path)
    Rails.logger.debug "FileParser:parse_files: .xlsx file parsing..."
    table_hash = Hash.new
    workbook = Roo::Spreadsheet.open(path)
    worksheets = workbook.sheets
    Rails.logger.debug "FileParser:parse_files: .xlsx worksheets: #{worksheets.inspect}"
    # Loop through sheets
    worksheets.each do |worksheet|
      #Rails.logger.debug "FileParser:parse_files: .xlsx worksheet: #{worksheet.inspect}"
      row_number = 0
      # Here begins all rows
      workbook.sheet(worksheet).each_row_streaming(pad_cells: true) do |row|
        row_hash = Hash.new #row results

        # Loop through cells
        cell_count = 1
        row_cells = row.map { |cell| !cell.nil? ? cell.value : nil }
        row_cells.each do |cell_value|
          row_hash[cell_count] = cell_value
          cell_count += 1
        end

        table_hash[row_number] = row_hash
        row_number += 1
      end
    end
    Rails.logger.debug "FileParser:parse_files: .xlsx result row count: #{table_hash.count}"
    return table_hash
  end

  def readdif(path)
    Rails.logger.debug "FileParser:parse_files: .dif file parsing..."
    table_hash = Hash.new
    file = Dif::Reader.new(path)
    if !file.nil?
      Rails.logger.debug "FileParser:parse_files: .dif file: #{file.inspect}"
      rows = file.export_csv.split("\n")
      row_number = 0
      if rows.any?
        rows.each do |row|
          #Rails.logger.debug "FileParser:parse_files: .dif row: #{row.inspect}"
          row_hash = Hash.new
          cell_count = 1
          cells = row.split("\t")

          cells.each do |cell_value|
            row_hash[cell_count] = cell_value
            cell_count += 1
          end
          table_hash[row_number] = row_hash
          row_number += 1
        end
      end
    end
    Rails.logger.debug "FileParser:parse_files: .xlsx result row count: #{table_hash.count}"
    return table_hash
  end

  def readtxt(path)
    table_hash = Hash.new
    row_number = 0
    File.readlines(path).each do |row|
      unless row.split("\n").empty?
        row_hash = Hash.new
        cell_count = 1
        cells = row.to_s.split("\t")

        cells.each do |cell_value|
          row_hash[cell_count] = cell_value
          cell_count += 1
        end
        table_hash[row_number] = row_hash
        row_number += 1
      end
    end
    return table_hash
  end
end
