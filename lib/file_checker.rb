class FileChecker
  def directories_to_search(farm_id = 0)
    # Only loged in user can search directories
    #farm_directories = Hash.new
    #if !current_user.nil?
    root_dir = APP_CONFIG['root_dir']
    Rails.logger.debug "ApplicationHelper:directories_to_search Searching root dir: #{root_dir}"
    if !root_dir.nil?
      unless farm_id <= 0
        root_dir += "/**/" + farm_id.to_s + "/*"
      else
        root_dir += "/**/*"
      end
      farm_directories = Dir.glob(root_dir)
    end
    #end
    return farm_directories
  end

  # Returns bull if file name is one of regex patterns from config file
  def check_file_name?(file_name)
    if !file_name.nil? && !file_name.empty?
      file_patterns = Array(APP_CONFIG['file_patterns']) + Array(APP_CONFIG['Predictions_reports'])
      file_patterns.each do |modelName, patternString|
        pattern = Regexp.new(patternString).freeze
        #returns true if object is nil, or not found
        if !pattern.match(file_name).nil?
          return true
        end
      end
    end
    return false
  end
end
