class AddIndexToGendersId < ActiveRecord::Migration[5.0]
  def change
    add_index :genders, :id, unique: true
  end
end
