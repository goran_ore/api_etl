class CreateGenstatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :genstatuses, :id =>false do |t|
      t.integer :id, :option =>'PRIMARY KEY'
      t.string :name

      t.timestamps
    end
  end
end
