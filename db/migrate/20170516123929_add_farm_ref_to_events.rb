class AddFarmRefToEvents < ActiveRecord::Migration[5.0]
  def change
    add_reference :events, :farm, foreign_key: true
  end
end
