class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.date :date
      t.string :name
      t.integer :animal_id
      t.string :animal_number
      t.date :birthdate
      t.string :diagnosis
      t.string :treatment
      t.string :drug
      t.string :technician
      t.string :bull_id
      t.string :bull_name
      t.string :exit_reason
      t.string :exit_type
      t.references :etl_process

      t.timestamps
    end
  end
end
