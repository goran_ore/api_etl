class CreateEtlProcesses < ActiveRecord::Migration[5.0]
  def change
    create_table :etl_processes do |t|
      t.references :farm, foreign_key: true
      t.references :user, foreign_key: true
      t.string :status
      t.datetime :started_at
      t.datetime :ended_at

      t.timestamps
    end
  end
end
