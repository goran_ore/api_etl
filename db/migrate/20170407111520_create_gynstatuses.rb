class CreateGynstatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :gynstatuses, :id => false do |t|
      t.string :name
      t.integer :id, :options => 'PRIMARY KEY'

      t.timestamps
    end
  end
end
