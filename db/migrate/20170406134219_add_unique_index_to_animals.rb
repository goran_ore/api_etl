class AddUniqueIndexToAnimals < ActiveRecord::Migration[5.0]
  def change
    add_index :animals, [:animal_number, :birthdate, :farm_id], :unique => true
  end
end
