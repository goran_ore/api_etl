class CreateStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :statuses, :id => false do |t|
      t.integer :id, :options => 'PRIMARY KEY'
      t.string :name

      t.timestamps
    end
  end
end
