class AddIndexToGynstatuses < ActiveRecord::Migration[5.0]
  def change
    add_index :gynstatuses, :id, unique: true
  end
end
