class AddGynstatusToAnimals < ActiveRecord::Migration[5.0]
  def change
     add_reference :animals, :gynstatus, foreign_key: true
  end
end
