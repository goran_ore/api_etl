class CreateAnimals < ActiveRecord::Migration[5.0]
  def change
    create_table :animals do |t|
      t.string :animal_number
      t.date :birthdate
      t.integer :farm_id
      t.references :etl_process

      t.timestamps
    end
  end
end
