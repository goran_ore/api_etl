class AddIndexToGenstatuses < ActiveRecord::Migration[5.0]
  def change
    add_index :genstatuses, :id, unique: true
  end
end
