class AddStatusToEtlProcesses < ActiveRecord::Migration[5.0]
  def change
    add_reference :etl_processes, :status, foreign_key: true
  end
end
