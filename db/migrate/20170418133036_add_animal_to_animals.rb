class AddAnimalToAnimals < ActiveRecord::Migration[5.0]
  def change
    add_column :animals, :lactation, :integer
    add_column :animals, :dim, :integer
    add_column :animals, :dcc, :integer
    add_column :animals, :days_open, :integer
    add_column :animals, :location, :integer
  end
end
