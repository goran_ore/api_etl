class AddGenderToAnimal < ActiveRecord::Migration[5.0]
  def change
    add_reference :animals, :gender, foreign_key: true
  end
end
