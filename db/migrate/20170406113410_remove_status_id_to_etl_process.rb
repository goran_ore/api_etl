class RemoveStatusIdToEtlProcess < ActiveRecord::Migration[5.0]
  def change
    remove_column :etl_processes, :status, :integer
  end
end
