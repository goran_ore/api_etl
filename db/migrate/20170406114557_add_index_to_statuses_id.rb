class AddIndexToStatusesId < ActiveRecord::Migration[5.0]
  def change
    add_index :statuses, :id, unique: true
  end
end
