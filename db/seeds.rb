# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(name: "Gabriel",
  email: "gabriel@gmail.com",
  password: "123456",
  password_confirmation: "123456",
  created_at: Time.now,
  admin: true,
  activated: true,
activated_at: Time.zone.now)

User.create!(name: "Goran",
  email: "goran.ore@gmail.com",
  password: "123456",
  password_confirmation: "123456",
  admin: true,
  activated: true,
activated_at: Time.zone.now)

User.create!(name: "Matea",
  email: "matea.ivanesic@gideonbros.net",
  password: "123456",
  password_confirmation: "123456",
  admin: true,
  activated: true,
activated_at: Time.zone.now)

User.create!(name: "Miroslav",
  email: "miroslav.jankovic@gideonbros.net",
  password: "123456",
  password_confirmation: "123456",
  admin: true,
  activated: true,
activated_at: Time.zone.now)

Farm.create!(name: "First farm", address: "Some address 1")
Farm.create!(name: "Second farm", address: "Some address 2")
Farm.create!(name: "Third farm", address: "Some address 3")

Status.create!(name: "Prepared", id: "1")
Status.create!(name: "In progress", id: "2")
Status.create!(name: "Finished", id: "3")
Status.create!(name: "Failed", id: "4")

Gynstatus.create!(name: "Open", id: "1")
Gynstatus.create!(name: "Pregnant", id: "2")
Gynstatus.create!(name: "Bred", id: "3")
Gynstatus.create!(name: "Fresh", id: "4")
Gynstatus.create!(name: "Aborted", id: "5")
Gynstatus.create!(name: "Bred (Aborted)", id: "6")
Gynstatus.create!(name: "Open (Aborted)", id: "7")
Gynstatus.create!(name: "Pregnant Dry", id: "8")
Gynstatus.create!(name: "Pregnant (1st Preg Check)", id: "9")

Genstatus.create!(id: "1", name: "Milk")
Genstatus.create!(id: "2", name: "Heifer")
Genstatus.create!(id: "3", name: "Heifer (pre)")
Genstatus.create!(id: "4", name: "Parking")
Genstatus.create!(id: "5", name: "Male")

Gender.create!(id: "1", name: "Male")
Gender.create!(id: "2", name: "Female")
