# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170516130328) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "animals", force: :cascade do |t|
    t.string   "animal_number"
    t.date     "birthdate"
    t.integer  "farm_id"
    t.integer  "etl_process_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "gender_id"
    t.integer  "genstatus_id"
    t.integer  "gynstatus_id"
    t.integer  "offspring_status_id"
    t.integer  "lactation_group_id"
    t.string   "sire_number"
    t.string   "dam_number"
    t.decimal  "entry_weight"
    t.date     "date"
    t.integer  "animal_id"
    t.integer  "lactation"
    t.integer  "dim"
    t.integer  "dcc"
    t.integer  "days_open"
    t.integer  "location"
    t.index ["animal_id"], name: "index_animals_on_animal_id", using: :btree
    t.index ["animal_number", "birthdate", "farm_id"], name: "index_animals_on_animal_number_and_birthdate_and_farm_id", unique: true, using: :btree
    t.index ["etl_process_id"], name: "index_animals_on_etl_process_id", using: :btree
    t.index ["gender_id"], name: "index_animals_on_gender_id", using: :btree
    t.index ["genstatus_id"], name: "index_animals_on_genstatus_id", using: :btree
    t.index ["gynstatus_id"], name: "index_animals_on_gynstatus_id", using: :btree
    t.index ["lactation_group_id"], name: "index_animals_on_lactation_group_id", using: :btree
    t.index ["offspring_status_id"], name: "index_animals_on_offspring_status_id", using: :btree
  end

  create_table "etl_processes", force: :cascade do |t|
    t.integer  "farm_id"
    t.integer  "user_id"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "status_id"
    t.index ["farm_id"], name: "index_etl_processes_on_farm_id", using: :btree
    t.index ["status_id"], name: "index_etl_processes_on_status_id", using: :btree
    t.index ["user_id"], name: "index_etl_processes_on_user_id", using: :btree
  end

  create_table "events", force: :cascade do |t|
    t.date     "date"
    t.string   "name"
    t.integer  "animal_id"
    t.string   "animal_number"
    t.date     "birthdate"
    t.string   "diagnosis"
    t.string   "treatment"
    t.string   "drug"
    t.string   "technician"
    t.string   "bull_id"
    t.string   "bull_name"
    t.string   "exit_reason"
    t.string   "exit_type"
    t.integer  "etl_process_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "bull_number"
    t.integer  "lactation"
    t.integer  "dim"
    t.date     "month_start"
    t.integer  "genstatus_id"
    t.integer  "farm_id"
    t.index ["etl_process_id"], name: "index_events_on_etl_process_id", using: :btree
    t.index ["farm_id"], name: "index_events_on_farm_id", using: :btree
    t.index ["genstatus_id"], name: "index_events_on_genstatus_id", using: :btree
  end

  create_table "farms", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "genders", id: false, force: :cascade do |t|
    t.integer  "id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_genders_on_id", unique: true, using: :btree
  end

  create_table "genstatuses", id: false, force: :cascade do |t|
    t.integer  "id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_genstatuses_on_id", unique: true, using: :btree
  end

  create_table "gynstatuses", id: false, force: :cascade do |t|
    t.string   "name"
    t.integer  "id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_gynstatuses_on_id", unique: true, using: :btree
  end

  create_table "statuses", id: false, force: :cascade do |t|
    t.integer  "id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_statuses_on_id", unique: true, using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  end

  add_foreign_key "animals", "genders"
  add_foreign_key "animals", "genstatuses"
  add_foreign_key "animals", "gynstatuses"
  add_foreign_key "etl_processes", "statuses"
  add_foreign_key "events", "farms"
  add_foreign_key "events", "genstatuses"
end
