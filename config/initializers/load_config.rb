#converts yaml file into a hash
# Use: someVariable = APP_CONFIG['somevalue']
APP_CONFIG = YAML.load_file("#{Rails.root}/config/config.yml")[Rails.env]
