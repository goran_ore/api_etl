source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.0.3'
gem 'pg'
gem 'puma', '~> 3.0'
gem 'jsonapi-resources'
gem 'bcrypt', '~> 3.1.11'
gem 'spreadsheet'
gem 'roo'
gem 'roo-xls'
gem 'dif'

# Part of token based authentication
gem 'jwt'
# The simple command gem is an easy way of creating services.
# Its role is similar to the role of a helper, but instead of facilitating the connection between the controller and the view,
# it does the same for the controller and the model. In this way, we can shorten the code in the models and controllers.
# Read more at https://www.pluralsight.com/guides/ruby-ruby-on-rails/token-based-authentication-with-ruby-on-rails-5-api#lVZcFIGMhgITMWZv.99
gem 'simple_command'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'pry-rails'
end

group :development do
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
