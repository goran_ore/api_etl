class EtlProcessesController < ApplicationController
  before_action :set_etl_process, only: [:show, :update, :destroy]

  # GET /etl_processes
  def index
    if !current_user.nil?
      logger.debug "EtlProcessController:index Starting to check for jobs...."
      if any_jobs?
        logger.debug "EtlProcessController:index Jobs found, starting setup_jobs..."
        setup_jobs
      else
        # List all processes
        @etl_processes = EtlProcess.where("user_id = ?", current_user.id)

        render json: { message: "Jobs found!" }
      end
    end
    # @etl_processes = EtlProcess.all
    # # logger.debug "EtlProcessesController: @etl_processes: #{@etl_processes.index}"
    # render json: @etl_processes
  end

  # GET /etl_processes/1
  def show
    render json: @etl_process
  end

  # POST /etl_processes
  def create
    if !current_user.nil?
      logger.debug "EtlProcessController:create Starting to check for jobs...."
      if any_jobs?
        errors_messages = Array.new
        logger.debug "EtlProcessController:create Jobs found, starting setup_jobs..."
        setup_jobs

        @etl_processes.each do |process|
          unless process.status_id == 3
            logger.debug "EtlProcessController:create Process initiating: #{process.inspect}"
            process.prepare_all
            process.initialize_data_models
            process.status_id = 2
            process.ended_at = Time.now
            logger.debug "EtlProcessController:create Process saving started..."
            if !process.save

              #This part ahead is error logging. Only first model will be displayed
              logger.debug "Process not saved because: #{process.errors.full_messages}"
              errors_messages.push("Process not saved because: #{process.errors.full_messages}")


              #EVENTS
              process.events.each do |event|
                unless event.valid?
                  errors_messages.push("event: #{event.inspect} is not valid!")
                  logger.debug "event: #{event.inspect} is not valid!"
                end
                break if !event.valid? #Only one message will be enough
              end

              #animalS
              process.animals.each do |animal|
                unless animal.valid?
                  errors_messages.push("animal: #{animal.inspect} is not valid!")
                  logger.debug "animal: #{animal.inspect} is not valid!"
                end
                break if !animal.valid? #Only one message will be enough
              end

              render json: { errors: errors_messages }
            else
              process.update_status(3)
              render json: { message: 'Files successfully imported into database!' }
            end
          end
        end
      else
        render json: { message: "No new files to process." }
      end
    end
    # @etl_process = EtlProcess.new(etl_process_params)
    #
    # if @etl_process.save
    #   render json: @etl_process, status: :created, location: @etl_process
    # else
    #   render json: @etl_process.errors, status: :unprocessable_entity
    # end
  end

  # PATCH/PUT /etl_processes/1
  def update
    if @etl_process.update(etl_process_params)
      render json: @etl_process
    else
      render json: @etl_process.errors, status: :unprocessable_entity
    end
  end

  # DELETE /etl_processes/1
  def destroy
    @etl_process.destroy
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_etl_process
    @etl_process = EtlProcess.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def etl_process_params
    params.require(:etl_process).permit(:farm_id, :user_id, :status, :started_at, :ended_at)
  end

  private
  #Parses path and extracts last directory name. If it is a integer number try and load Farm by it.
  def farm_by_path(dirPath)
    if !dirPath.nil? && !dirPath.parent.nil?
      farm_id = dirPath.parent.basename.to_s.to_i
      if 0 < farm_id
        Farm.find(dirPath.parent.basename.to_s.to_i)
      end
    end
  end

  # Checks if there are any new files in folders to process.
  # Each farms path is individualy checked,
  # and fills a list (@farmsForProcessing) of farms to process.
  # Returns true if there are jobs.
  def any_jobs?
    @farm_directories = FileChecker.new.directories_to_search
    logger.debug "EtlProcessController::any_jobs Farm directories found: #{@farm_directories}"
    @farms = Array.new()
    @farm_directories.each do |dir|
      dirPath = Pathname.new(dir)
      # If file path is not just a directory
      if !dirPath.directory?
        # now check if that file name apply with regex match from configuration
        if FileChecker.new.check_file_name?(dirPath.basename.to_s)
          # Since file is here for processing, add farm to list
          farm = farm_by_path(dirPath)
          if !@farms.include?(farm)
            @farms.push(farm)
          end
        end
      end
    end
    # Returns true if any farm have job to do
    return !@farms.empty?
  end

  # By the @farmsForProcessing list
  # create each etl process by farm in "Scheduled" status.
  def setup_jobs
    if !@farms.empty?
      @farms.each do |farm|
        logger.debug "EtlProcessController:setup_jobs Setup jobs started for farm_id: #{farm.id}..."
        # Load processes if not already loaded. Need activerecord collection
        if @etl_processes.nil?
          @etl_processes = EtlProcess.where("user_id = ?", current_user.id)
          logger.debug "EtlProcessController:setup_jobs @etl_processes loaded!"
        end

        unless @etl_processes.any? {|f| f.farm == farm && f.status_id == 1}
          logger.debug "EtlProcessController:setup_jobs Process initialization started..."
          process = @etl_processes.build(user: current_user, farm: farm, started_at: Time.now, status_id: "1")
          process.save()
          logger.debug "EtlProcessController:setup_jobs Etl process saved with status 1. Preparation starting..."
          process.prepare_all
          # process.save()
          # @etl_processes.push(process)
        end
      end
    end
  end
end
