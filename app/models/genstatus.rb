class Genstatus < ApplicationRecord
 has_many :animals
 has_many :events
 validates :id, presence: true
 validates :name, presence: true
 self.primary_key = "id"
end
