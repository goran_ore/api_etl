class Status < ApplicationRecord
  self.primary_key = "id"
  has_many :etl_processes
  validates :id, presence: true
  validates :name, presence: true
end
