class Event < ApplicationRecord
  belongs_to :etl_process
  belongs_to :farm
  belongs_to :animal, optional: true
  belongs_to :genstatus, optional: true
  validates :date, presence: true
  validates :name, presence: true, length: {maximum: 100}
  validates :animal_number, presence: true
end
