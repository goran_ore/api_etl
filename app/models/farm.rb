class Farm < ApplicationRecord
  has_many :etl_processes
  has_many :users, through: :etl_processes
  has_many :events
  has_many :animals

  validates :name, presence: true, length: {maximum: 50}
  validates :address, length: {maximum: 255}
end
