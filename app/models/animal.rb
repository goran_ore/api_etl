class Animal < ApplicationRecord
  belongs_to :gender, optional: true
  belongs_to :etl_process
  belongs_to :genstatus, optional: true
  belongs_to :lactation_group, optional: true
  belongs_to :offspring_status, optional: true
  belongs_to :gynstatus, optional: true
  belongs_to :farm
  belongs_to :parent, class_name: 'Animal', foreign_key: 'animal_id', optional: true
  has_many :children, class_name: 'Animal', foreign_key: 'animal_id'
  has_many :events
  has_many :milk_tests

  validates :animal_number, uniqueness: {scope: [:farm_id, :birthdate]}
  validates :birthdate, presence: true
  validates :farm, presence: true
  validates :animal_number, presence: true
  #validates :gender, presence: true
  validates :etl_process, presence: true
  #validates :genstatus, presence: true
  #validates :gynstatus, presence: true

  # validates :animal_id, presence: true
  # validates :sire_number, presence: true
  # validates :dam_number, presence: true
  # validates :entry_weight, presence: true
  # validates :date, presence: true
end
