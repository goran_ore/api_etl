class Gender < ApplicationRecord
  has_many :animals
  validates :id, presence: true
  validates :name, presence: true
  self.primary_key = "id"
end
