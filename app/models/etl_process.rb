class EtlProcess < ApplicationRecord
  after_initialize :initialize_variables
  after_save :backup_files

  belongs_to :user
  belongs_to :farm
  belongs_to :status
  has_many :events, :dependent => :destroy
  has_many :animals, :dependent => :destroy
  has_many :milks, :dependent => :destroy
  has_many :reproduction_cycles, :dependent => :destroy
  has_many :milk_tests, :dependent => :destroy
  has_many :herd_months, :dependent => :destroy
  has_many :bulls, :dependent => :destroy
  has_many :predictions, :dependent => :destroy
  has_many :temperatures, :dependent => :destroy
  has_many :financial_months

  before_save { started_at = 1.minute.ago }

  validates :user_id, presence: true
  #validates :farm_id, presence: true
  validates :started_at, presence: true#, :timeliness => {:on_or_after => lambda { (1.day.ago) }}
  validates :status_id, presence: true

  # Contains mappings of expecting column name and column index in document.
  EVENT_COLUMN_MAPPER = { "date": 2, "name": 1, "animal_number": 3, "birthdate": 5,
    "diagnosis": 15, "treatment": 16, "drug": 17, "technician": 18,
    "bull_id": 11, "bull_name": 12, "exit_reason": 20, "exit_type": 19,
    "general_status": 4, "dim": 8, "lactation": 6
  }

  ANIMAL_REPORT_COLUMN_MAPPER = { "lactation": 3, "dim": 4,"dcc": 9, "days_open": 10, "location": 2, "birthdate": 6,
    "dam_number": 8,"animal_number": 1,"genstatus_number": 7, "gynstatus_number": 11,
    "sex": 5, "gynstatus_number2": 12
  }

  EVENT_SUMMARY_COLUMN_MAPPER = {"date": 3, "animal_number": 1, "event": 2, "data1": 5, "data2": 6 }

  def initialize_variables()
    # Variables needed only for new or in progress processes.
    if self.status_id < 3
      @model_paths = Hash.new
      @initialized_models = Hash.new
      @parser = FileParser.new
      started_at = 1.minute.ago

      # Generic table variables
      @@genstatuses = Genstatus.all
      @@gynstatuses = Gynstatus.all
      @@genders = Gender.all
      @@animals_imported = nil
    end
  end

  def prepare_all
    self.group_paths_by_model
    logger.debug "EtlProcess:prepare_all Paths found: #{@model_paths}. Parsing starting..."
    self.parse_files
    #self.initialize_data_models
  end

  # Load animals for given farm to check existance in various model initializations
  def load_existing_animals(reload = false)
    # Load only if forced, or @@animals_imported is empty
    if !reload || @@animals_imported.nil?
      @@animals_imported = Animal.where(farm: self.farm)
    end
  end

  #move files to backup location
  def backup_files
    # Backup only finished process
    if self.status_id == 2
      backup_path = ""
      logger.debug "backup_path_first: #{backup_path}"
      unless @model_paths.nil?
        backup_path = APP_CONFIG['backup_path'].to_s + '/' + self.farm.id.to_s
        logger.debug "backup_path_second: #{backup_path}"
        if !File.directory?(backup_path)
          logger.debug "Creating backup direcotry: #{backup_path}"
          FileUtils.mkdir_p(backup_path)
        end
        @model_paths.values.flatten.uniq.each do |path|
          logger.debug "Backing up files: #{path} to: #{backup_path}"
          FileUtils.move(path, backup_path)
        end
      end
    end
  end

  # Updates status of processing.
  def update_status(status_id = 0)
    if (1..4).include? status_id
      self.status = Status.find(status_id)
      update_attribute(:status, status)
    end
  end

  # Returns hash model->Array(<paths>)
  def group_paths_by_model
    # @model_paths = Hash.new
    farm_files = FileChecker.new.directories_to_search(farm.id)
    ar1 =APP_CONFIG['file_patterns']
    ar2= APP_CONFIG['Predictions_reports']
    file_patterns = [*ar1,*ar2]
    farm_files.each do |path|
      file_patterns.each do |modelName, patternString|
        pattern = Regexp.new(patternString).freeze
        if !pattern.match(path).nil?
          if !@model_paths.has_key?(modelName)
            @model_paths[modelName] = Array.new
          end
          # Check file is added
          if !@model_paths[modelName].include?(path)
            @model_paths[modelName].push(path)
          end
        end
      end
    end
    return @model_paths
  end

  # Check if there is any prepared models.
  def any_jobs?
    self.events.any? || self.animals.any?
  end

  #Initializes all data models by their name and fill them with results, prepare for save.
  def initialize_data_models
    # @initialized_models = Hash.new
    unless @model_paths.nil?
      # First, initialize Animals because they will be used by other models
      if @model_paths.has_key?("Animal")
        @initialized_models["Animal"] = initialize_many_animals(@parser.get_results(@model_paths["Animal"]))
      end
      @model_paths.each do |modelName, paths|
        # Initialize model by name and results in it
        if !@initialized_models.has_key?(modelName)

          case modelName
          when "Event"
            @initialized_models[modelName] = initialize_many_events(@parser.get_results(paths))
          when "Animal_report"
            @initialized_models[modelName] = initialize_many_animal_reports(@parser.get_results(paths))
          when "Event_summary"
            @initialized_models[modelName] = initialize_many_event_summary(@parser.get_results(paths))
          end
        end
      end
    end
    return @initialized_models
  end

  # Initiate parsing of results
  def parse_files
    unless @model_paths.nil?
      @parser = FileParser.new(@model_paths.values.flatten)
      #extracts all paths from collections in value of a hash
      # you could use also map(&:last) instead of values
    end
  end

  #Initializes collection of Event models by their results from parser
  #Before calling initializer, row to send will have to be changed.
  #No extra rules have been defined for event models.
  #Returns a list of initialized models
  def initialize_many_events(results)
    unless results.nil?
      # Since it is only one row, go through them and initialize one by one
      results.each do |result|
        result.each do |key, row|
          date = Date.parse(row[EVENT_COLUMN_MAPPER[:date]].to_s) rescue nil
          if 0 < key.to_i && !date.nil?
            event = self.events.build(farm: self.farm)
            event.date = date
            event.name = row[EVENT_COLUMN_MAPPER[:name]].to_s
            event.animal_number = row[EVENT_COLUMN_MAPPER[:animal_number]].to_s
            event.lactation = row[EVENT_COLUMN_MAPPER[:lactation]].to_s.strip != "--" ? row[EVENT_COLUMN_MAPPER[:lactation]].to_i : nil
            event.dim = row[EVENT_COLUMN_MAPPER[:dim]].to_s.strip != "--" ? row[EVENT_COLUMN_MAPPER[:dim]].to_i : nil
            event.birthdate = Date.parse(row[EVENT_COLUMN_MAPPER[:birthdate]].to_s) rescue nil
            event.diagnosis = row[EVENT_COLUMN_MAPPER[:diagnosis]].to_s != "--" ? row[EVENT_COLUMN_MAPPER[:diagnosis]].to_s : nil
            event.treatment = row[EVENT_COLUMN_MAPPER[:treatment]].to_s != "--" ? row[EVENT_COLUMN_MAPPER[:treatment]].to_s : nil
            event.drug = row[EVENT_COLUMN_MAPPER[:drug]].to_s != "--" ? row[EVENT_COLUMN_MAPPER[:drug]].to_s : nil
            event.technician = row[EVENT_COLUMN_MAPPER[:technician]].to_s != "--" ? row[EVENT_COLUMN_MAPPER[:technician]].to_s : nil
            event.bull_id = row[EVENT_COLUMN_MAPPER[:bull_id]].to_s != "--" ? row[EVENT_COLUMN_MAPPER[:bull_id]].to_s : nil
            event.bull_name = row[EVENT_COLUMN_MAPPER[:bull_name]].to_s != "--" ? row[EVENT_COLUMN_MAPPER[:bull_name]].to_s : nil
            event.exit_reason = row[EVENT_COLUMN_MAPPER[:exit_reason]].to_s != "--" ? row[EVENT_COLUMN_MAPPER[:exit_reason]].to_s : nil
            event.exit_type = row[EVENT_COLUMN_MAPPER[:exit_type]].to_s != "--" ? row[EVENT_COLUMN_MAPPER[:exit_type]].to_s : nil
            event.month_start = date.beginning_of_month
            event.genstatus = @@genstatuses.select { |gen| gen.name == row[EVENT_COLUMN_MAPPER[:general_status]].to_s.strip }[0] #Genstatus.find_by(name: row[EVENT_COLUMN_MAPPER[:general_status]].to_s.strip)

            # Connect with animal that is to be imported
            if self.animals.select{|animal| animal.animal_number.to_s == row[EVENT_COLUMN_MAPPER[:animal_number]].to_s && animal.farm.id == self.farm.id}.any?
              event.animal = self.animals.select{|animal| animal.animal_number.to_s == row[EVENT_COLUMN_MAPPER[:animal_number]].to_s && animal.farm.id == self.farm.id}[0]
            elsif self.farm.animals.exists?(:animal_number == row[EVENT_COLUMN_MAPPER[:animal_number]].to_s)
              event.animal = self.farm.animals.find_by(:animal_number == row[EVENT_COLUMN_MAPPER[:animal_number]].to_s)[0]
            end
          end
        end
      end
    end
  end

  def initialize_many_animals(results)
    unless results.nil?
      load_existing_animals
      # Since it is only one row, go through them and initialize one by one
      results.each do |result|
        result.each do |key, row|
          if 0 < key.to_i
            # Flag to check if animal is already added for import from this or any other report
            animal_added = self.animals.select{|animal| animal.animal_number.to_s == row[EVENT_COLUMN_MAPPER[:animal_number]].to_s &&
              animal.birthdate == row[EVENT_COLUMN_MAPPER[:birthdate]] &&
            animal.farm.id == self.farm.id}.any?
            animal_exists = @@animals_imported.select{|animal| animal.animal_number.to_s == row[EVENT_COLUMN_MAPPER[:animal_number]].to_s &&
              animal.birthdate == row[EVENT_COLUMN_MAPPER[:birthdate]] &&
            animal.farm.id == self.farm.id}.any?
            #logger.debug "Animal_number to add: #{row[EVENT_COLUMN_MAPPER[:animal_number]].to_s}; animal_added: #{animal_added}; animal_exists: #{animal_exists}"
            if !animal_added && !animal_exists
              animal = self.animals.build(animal_number: row[EVENT_COLUMN_MAPPER[:animal_number]],
              birthdate: row[EVENT_COLUMN_MAPPER[:birthdate]], farm: self.farm)
            end
          end
        end
      end
    end
  end

  def initialize_many_animal_reports(results)
    unless results.nil?
      # Since it is only one row, go through them and initialize one by one
      date = Date.today
      results.each do |result|
        result.each do |key, row|
          #Extract from third column
          if 0 == key.to_i
            date = Date.parse(row[3].to_s, "%Y.%m.%d") rescue nil
          end
          if 4 < key.to_i
            animal = self.animals.build(farm: self.farm)
            animal.animal_number = row[ANIMAL_REPORT_COLUMN_MAPPER[:animal_number]].to_i
            animal.lactation = row[ANIMAL_REPORT_COLUMN_MAPPER[:lactation]].to_i
            animal.dim = row[ANIMAL_REPORT_COLUMN_MAPPER[:dim]].to_s.strip != "-" ? row[ANIMAL_REPORT_COLUMN_MAPPER[:dim]].to_i : nil
            animal.dcc = row[ANIMAL_REPORT_COLUMN_MAPPER[:dcc]].to_i
            animal.days_open = row[ANIMAL_REPORT_COLUMN_MAPPER[:days_open]].to_s.strip != "-" ? row[ANIMAL_REPORT_COLUMN_MAPPER[:days_open]].to_i : nil
            animal.location = row[ANIMAL_REPORT_COLUMN_MAPPER[:location]].to_i
            animal.birthdate = Date.parse(row[ANIMAL_REPORT_COLUMN_MAPPER[:birthdate]].to_s, "%Y.%m.%d") rescue nil
            #check if date is in the future and try to referse format
            if animal.birthdate > date
              animal.birthdate = Date.strptime(row[ANIMAL_REPORT_COLUMN_MAPPER[:birthdate]].to_s, "%d.%m.%Y") + 2000.years rescue nil
            end
            animal.date = date
            animal.dam_number = row[ANIMAL_REPORT_COLUMN_MAPPER[:dam_number]].to_s.strip != "-" ? row[ANIMAL_REPORT_COLUMN_MAPPER[:dam_number]].to_s : nil
            animal.gender = @@genders.select { |gen| gen.name == row[ANIMAL_REPORT_COLUMN_MAPPER[:sex]].to_s.strip }[0] #Gender.find_by(name: row[ANIMAL_REPORT_COLUMN_MAPPER[:sex]].to_s.strip)

            # gynecological status is sometimes moved to two columns that need to be joined together
            gynStatus = (row[ANIMAL_REPORT_COLUMN_MAPPER[:gynstatus_number]].to_s.strip + " " + row[ANIMAL_REPORT_COLUMN_MAPPER[:gynstatus_number2]].to_s.strip).strip
            animal.gynstatus = @@gynstatuses.select { |gyn| gyn.name == gynStatus }[0] # Gynstatus.find_by(name: gynStatus)

            # general status is confirmation of In Milk by Yes or in gynecological column for Heifer
            genStatusConfirm = row[ANIMAL_REPORT_COLUMN_MAPPER[:genstatus_number]].to_s.strip
            if genStatusConfirm.downcase == "yes"
              animal.genstatus = @@genstatuses.select { |gen| gen.name == "Milk" }[0] #Genstatus.find_by(name: "Milk")
            elsif gynStatus.strip.downcase == "heifer"
              animal.genstatus = @@genstatuses.select { |gen| gen.name == "Heifer" }[0] #Genstatus.find_by(name: "Heifer")
            end

            animal.farm = self.farm

            # Connect with parent that is to be imported
            unless animal.dam_number.nil?
              if self.animals.select{|parent| parent.animal_number.to_s == animal.dam_number && parent.farm.id == self.farm.id}.any?
                animal.parent = self.animals.select{|parent| parent.animal_number.to_s == animal.dam_number && parent.farm.id == self.farm.id}[0]
              elsif !@@animals_imported.nil? && @@animals_imported.select{|parent| parent.animal_number.to_s == animal.dam_number}.any?
                animal.parent = @@animals_imported.select{|parent| parent.animal_number.to_s == animal.dam_number}[0]
              end
            end
          end
        end
      end
    end
  end

  def initialize_many_event_summary(results)
    unless results.nil?
      load_existing_animals
      results.each do |result|
        result.each do |key, row|
          if 5 < key.to_i

            eventName = row[EVENT_SUMMARY_COLUMN_MAPPER[:event]].to_s.strip
            unless "change group" == eventName.downcase  #Here every event that is not "Change group" will be build in model
              event = self.events.build(farm: self.farm)
              event.date = Date.parse(row[EVENT_SUMMARY_COLUMN_MAPPER[:date]].to_s, "%Y.%m.%d") rescue nil
              event.animal_number = row[EVENT_SUMMARY_COLUMN_MAPPER[:animal_number]].to_s.strip
              event.name = eventName
              if "insemination" == eventName.downcase
                event.bull_name = row[EVENT_SUMMARY_COLUMN_MAPPER[:data1]].to_s
                event.technician = row[EVENT_SUMMARY_COLUMN_MAPPER[:data2]].to_s != "-" ? row[EVENT_SUMMARY_COLUMN_MAPPER[:data2]].to_s : nil
              elsif "ailment" == eventName.downcase
                event.diagnosis = row[EVENT_SUMMARY_COLUMN_MAPPER[:data1]].to_s
                event.treatment = row[EVENT_SUMMARY_COLUMN_MAPPER[:data2]].to_s != "-" ? row[EVENT_SUMMARY_COLUMN_MAPPER[:data2]].to_s : nil
              end

              unless event.animal_number.nil?
                if self.animals.select{|animal| animal.animal_number.to_s == event.animal_number.to_s && animal.farm.id == self.farm.id}.any?
                  event.animal = self.animals.select{|animal| animal.animal_number.to_s == event.animal_number.to_s && animal.farm.id == self.farm.id}[0]
                elsif !@@animals_imported.nil? && @@animals_imported.select{|animal| animal.animal_number.to_s == event.animal_number.to_s}.any?
                  event.animal = @@animals_imported.select{|animal| animal.animal_number.to_s == event.animal_number.to_s}[0]
                end
              end
            end
          end
        end
      end
    end
  end
end
